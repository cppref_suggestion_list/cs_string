/***********************************************************************
*
* Copyright (c) 2017-2022 Barbara Geller
* Copyright (c) 2017-2022 Ansel Sermersheim
*
* This file is part of CsString.
*
* CsString is free software, released under the BSD 2-Clause license.
* For license details refer to LICENSE provided with this project.
*
* CopperSpice is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
*
* https://opensource.org/licenses/BSD-2-Clause
*
***********************************************************************/

#define CATCH_CONFIG_EXPERIMENTAL_REDIRECT

#include <catch2/catch.hpp>

namespace Catch {

}
